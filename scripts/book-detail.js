﻿var bookdetail = angular.module('bookDetail', ['ngRoute']);

bookdetail.component('bookDetail', {
    templateUrl: 'book-detail.template.html',
    controller: ['$routeParams', '$http', function ($routeParams, $http) {
        var self = this;
        var bookId = $routeParams.bookId;

        $http.get('https://netology-fbb-store-api.herokuapp.com/book/' + bookId).
            success(function (data) {
                self.book = data;
            });
    }]
});
