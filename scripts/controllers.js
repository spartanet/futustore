﻿var futustore = angular.module('futustore', ['ngSanitize', 'ngRoute', 'bookList', 'bookDetail', 'about']);
futustore.config(['$locationProvider', '$routeProvider',
        function config($locationProvider, $routeProvider) {
            $locationProvider.hashPrefix('!');

            $routeProvider.
              when('/books', { template: '<book-list></book-list>' }).
              when('/books/:bookId', { template: '<book-detail></book-detail>' }).
              when('/about', { template: '<about></about>' }).
              when('/makeorder/:bookId', { template: '<order-form></order-form>' }).
              otherwise('/books');
        }
]);

futustore.controller(
    'primaryCtrl',
    ['$scope', '$route',
    function ($scope, $route) {
        $scope.$route = $route;
        $scope.isPath = function(path) {
            return (this.$route.current != undefined && this.$route.current.$$route != undefined) ? this.$route.current.$$route.originalPath != path : false;
        }
    }]
);
