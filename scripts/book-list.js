﻿var booklist = angular.module('bookList', ['ngRoute']);

booklist.component('bookList',
    {
        templateUrl: 'book-list.template.html',
        controller: [
            '$scope',
            '$http',
            function bookListCtrl($scope, $http) {
                var self = this;

                $http.get('https://netology-fbb-store-api.herokuapp.com/book/').success(function (data) {
                    self.bookList = data;
                });
            }
        ]
    });
